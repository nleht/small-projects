#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 18 16:58:17 2018

For use with MoM version 7 (or later)

@author: nle003
"""

#%% Preliminaries
import numpy as np
import matplotlib.pyplot as plt
import method_of_moments as MoM
import time

#%%  Auxiliary functions
class sfloat(float):
    "Short-print of floats"
    def __str__(self):
        return "%0.5g" % self.real
    def __repr__(self):
        return self.__str__()
    
def latex_float(f,digits=5):
    s = '-' if (f<0) else ''
    float_str = ('{0:.'+str(int(abs(digits)))+'g}').format(abs(f))
    if "e" in float_str:
        base, exponent = float_str.split("e")
        basef = '' if base=='1' else r'{:} \times'.format(base)
        return s + basef + r'10^{{{:}}}'.format(int(exponent))
    else:
        return s + float_str

def rods(Z1s, Z2s, Rs, verbose=0, dmax=.2):
    """Create and analyze the conductor object: cylinder of radius 1 and tip-to-tip length 2*L.
    The shape of the ends is given by string config in ['open','flat','cap']
    For details, see help on MoM.rod """
    zb = np.array([]); rb = np.array([])
    for Z1,Z2,R in zip(Z1s,Z2s,Rs):
        zb1,rb1 = MoM.rod((Z2-Z1)/2/R,'cap', dmax=dmax, verbose=verbose)
        zb1 *= R; rb1 *= R;
        zb = np.hstack((zb, zb1 + (Z1+Z2)/2, np.nan))
        rb = np.hstack((rb, rb1, np.nan))
    # Remove the last nan
    zb = zb[:-1]; rb = rb[:-1]
    return zb, rb

def multirod_conductor(zb, rb, connect=None, verbose=0, memorylimit=5000):
    #print(zb,rb)
    if len(zb)-1>memorylimit:
        raise Exception('Too many elements, stopping')
    #zb,rb = MoM.rod(L,config,dmax=0.25,verbose=verbose)
    t0 = time.time()
    Np = np.sum(np.isnan(zb))+1
    N = np.sum(np.isfinite(zb))-Np
    print('Number of intervals =',N,'; number of conductors =',Np)
    conductor = MoM.CylSymConductor(zb, rb, connect=connect, Nsub=3, verbose=verbose)
    t1 = time.time()
    if verbose>0:
        print('Created a cap conductor with',conductor.N,'elements, it took',sfloat(t1-t0),
              'seconds',flush=True)
    conductor.fill_matrix()
    t2 = time.time()
    if verbose>0:
        print('Filled the matrix, it took',sfloat(t2-t1),'seconds',flush=True)
    conductor.solve()
    t3 = time.time()
    if verbose>0:
        print('Solved the matrix, it took',sfloat(t3-t2),'seconds',flush=True)
    return conductor

def multirod_field(conductor, x):
    return conductor.Ee + np.array([conductor.E_axis(x1) for x1 in x])


def multirod_phi(conductor, x):
    return np.array([conductor.pot_axis(x1) for x1 in x]) - conductor.Ee*x

#%%
print('*** Two conductors ***')
Rs = [.5, 2] # radii
x = np.linspace(-10,10,10000)
save_plots = False
Ee = 1
Z1s = [-9.5, .5] # left ends
Z2s = [-.5, 9.5] # right ends
Qs = 1e3*(np.random.rand(2)-.5)
# Three different ways to calculate the charge transfer
zb, rb = rods(Z1s,Z2s,Rs,verbose=2,dmax=2)
c2 = multirod_conductor(zb, rb, verbose=2) # two disconnected rods
c2.Ee = Ee # set the field
c2.Q = Qs # set the charges of conductor pieces
plt.figure()
plt.clf()
line,=plt.plot(c2.zb, c2.rb)
line.set_label(r'$r(x)/a$')
line,=plt.plot(x,multirod_field(c2,x))
line.set_label(r'$E_x/E_0$')
plt.grid(True)
plt.xlabel(r'$x/a$')
plt.ylabel(r'$E_x/E_0$ on axis; $r/a$')
plt.title(r'Two capped cylinders, $Q_k/(E_0a^2\varepsilon_0)=$'+
               latex_float(Qs[0])+', '+latex_float(Qs[1]) )

#%% 1. The hard way - calculate at which charges the potentials equilibrate
Qav = np.sum(Qs)/2
A = np.hstack((c2.Cp_inv @ np.array([[1],[-1]]), np.array([[1],[1]]) ))
Qt1, phit1 = np.linalg.solve(A, c2.Phi_from_Ee*Ee + Qav*c2.Cp_inv @ np.array([1,1]) )
Qnew1 = Qav + Qt1*np.array([-1,1])

#%% 2. The provided way for short-circuiting
Qnew2, phit2 = c2.short_circuit([0,0]) # this changes c2.Q to Qnew!
line,=plt.plot(x,multirod_field(c2,x))
line.set_label(r'$E_x/E_0$ after connecting')
plt.legend()


#%% 3. Solve the same but connecting the conductors in the beginning
zb, rb = rods(Z1s,Z2s,Rs,verbose=2,dmax=2)
c1 = multirod_conductor(zb, rb, connect=[0,0], verbose=2) # connected
c1.Ee = 1
c1.Q = np.array([np.sum(Qs)])
Qnew3 = c2.Pmat @ c1.q # use our knowledge from previous calculation how to separate conductors
phit3 = c1.Phi

print('Charges: ', Qnew1, Qnew2, Qnew3)
print('Potential: ', phit1, phit2, phit3)

