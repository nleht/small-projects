# Short Projects

This repository contains various projects which are too small to have their own repositories but also are too large to be Snippets.

The files are protected by copyright. If you would like to use them to obtain results for a publication or a public presentation, please contact the [author](@nleht) at ![his email](email.png).

Some of the packages may use each other. Many of them use the _Utilities_ package.
