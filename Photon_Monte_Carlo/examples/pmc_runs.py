#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 12 14:53:11 2021

@author: nle003
"""
import time
import random
import numpy as np
from matplotlib import pyplot as plt
#import photon_monte_carlo as PMC
from photon_monte_carlo import PhotonSwarm, mc2eV
from utils import neat_float, ave, anti_diff, print_header, meandrize, duplicate

def print_oh(*args, **kws):
    "Entertain myself"
    color = random.choice(['red','green', 'yellow', 'blue', 'magenta', 'cyan'])
    kws.setdefault('flush', True)
    print_header(*args, color=color, bold=True, linewidth=100, **kws)

#%% Set it up!
Hsat = 600e3 # RHESSI is 600 km, ISS is 400 km
h0 = 15e3
N0 = int(1e7)
En0 = None #1e6/mc2eV # will not be used
c0 = -1 #np.cos(45*np.pi/180)
c1 = 1 # np.cos(170*np.pi/180)
Nruns = 20
altitude_bins = np.arange(0,100,0.1)*1e3

# The bremsstrahlung thermal spectrum
En_b = np.logspace(-1, 2.2, 1000)
En_c = ave(En_b)
dEn = np.diff(En_b)
f = np.exp(-En_c/(7.4e6/mc2eV))/En_c
F = anti_diff(0, f*dEn)
F = F/F[-1]

#%% Run it!
tstart = time.time()
Eini = 0
phec_dep = 0
pair_dep = 0
comp_dep = 0
rsat = np.ndarray((2,0), dtype=np.float64)
tsat = np.array([])
Esat = np.array([])
rground = rsat.copy()
tground = tsat.copy()
Eground = Esat.copy()
for krun in range(Nruns):
    print_oh('Run #', krun+1, '/', Nruns)
    if not np.isscalar(En0):
        En0 = np.interp(np.random.rand(N0), F, En_b)
    swarm = PhotonSwarm(N0, En0, h0, Hsat, costh_interval=[c0, c1], propagate_pair=True)
    swarm.run(verbose=True)

    comp_dep += np.histogram(swarm.compton_altitudes, weights=swarm.compton_energies,
                             bins=altitude_bins)[0]
    iphec = (swarm.status==PhotonSwarm.status_dict['phec'])
    ipair = (swarm.status==PhotonSwarm.status_dict['pair'])
    phec_dep += np.histogram(swarm.r[2][iphec], bins=altitude_bins, weights=swarm.En[iphec])[0]
    pair_dep += np.histogram(swarm.r[2][ipair], bins=altitude_bins, weights=swarm.En[ipair])[0]

    isat = (swarm.status==PhotonSwarm.status_dict['sat'])
    rsat = np.hstack((rsat, swarm.r[0:2,isat]))
    tsat = np.hstack((tsat, swarm.t[isat]))
    Esat = np.hstack((Esat, swarm.En[isat]))
    
    iground = (swarm.status==PhotonSwarm.status_dict['ground'])
    rground = np.hstack((rground, swarm.r[0:2,iground]))
    tground = np.hstack((tground, swarm.t[iground]))
    Eground = np.hstack((Eground, swarm.En[iground]))
    
    Eini += (En0*N0 if np.isscalar(En0) else np.sum(En0))
    print('Nsat =', np.sum(isat), 'Nground =', np.sum(iground))

ttot = time.time()-tstart
Nini = N0*Nruns

#%% Analysis
Nsat = len(Esat)
Nground = len(Eground)
print('Nsat =', Nsat, 'Nground =', Nground)
dsat = np.sqrt(np.sum(rsat**2, axis=0))
frac_sat = np.sum(Esat)/Eini
frac_ground = np.sum(Eground)/Eini
frac_phec = np.sum(phec_dep)/Eini
frac_pair = np.sum(pair_dep)/Eini
frac_comp = np.sum(comp_dep)/Eini
print('Sanity check: total =', frac_phec+frac_pair+frac_comp+frac_sat+frac_ground)

def to_deg(c):
    return neat_float(180*np.arccos(c)/np.pi)

print_oh('Result for En =', (En0*mc2eV/1e6 if np.isscalar(En0) else '[ThermBrems]'),
      'MeV, h =', h0/1e3, 'km, angles from', to_deg(c1), 'to', to_deg(c0), 'degrees')
print('Calculation time =', neat_float(ttot/60), 'min')
if Nsat > 0:
    print('Angle =', neat_float(np.arctan(np.mean(dsat)/Hsat)*180/np.pi), 'degrees')
print('Fraction reaching sat =', neat_float(Nsat/Nini))
print('Fraction of energy going to sat =', neat_float(frac_sat))
if np.isscalar(En0):
    iorig = (Esat==En0)
    Norig = np.sum(iorig)
    if Norig>0:
        print('Angle [orig En] =', neat_float(np.arctan(np.mean(dsat[iorig])/Hsat)*180/np.pi),
              'degrees')
        print('Fraction going to sat in orig En =', neat_float(Norig/Nini))
        print('Energy remaining in orig (fraction) =', neat_float(Nini*En0/np.sum(Esat)))
print('Fraction reaching ground =', neat_float(Nground/Nini))
print('Fraction of energy going to ground =', neat_float(frac_ground))

#% Plot the results
#plt.plot(rsat[iorig], tsat[iorig], '.')
# fig = plt.figure(1)
# fig.clear()
# ax = fig.add_subplot(projection='3d')
# isat_no = isat & (swarm.En!=En0)
# ax.scatter(swarm.px[isat_no], swarm.py[isat_no], swarm.pz[isat_no], marker='.')

#%% Absorbed energy
plt.figure(2)
plt.clf()
bins = altitude_bins[:-1]/1e3
if False:
    width = np.diff(altitude_bins)/1e3
    plt.bar(bins, comp_dep, width=width, align='edge')
    plt.bar(bins, phec_dep, width=width, align='edge')
    plt.bar(bins, pair_dep, width=width, align='edge')
else:
    # Simpler
    plt.semilogy(bins, np.vstack((comp_dep, phec_dep, pair_dep)).T, drawstyle='steps-post')
plt.grid(True)
plt.xlabel('h, km')
plt.title('Energy deposition')
plt.legend(['Compton', 'Photoelectric', 'Pair production'])

#%%
a_deg = np.arctan(dsat/Hsat)*180/np.pi
a_str = [r'$\alpha<20^\circ$', r'$20^\circ<\alpha<40^\circ$', r'$40^\circ<\alpha<60^\circ$']
ia = [(a_deg<20), (a_deg>=20) & (a_deg<40), (a_deg>=40) & (a_deg<60)]
plt.figure(3)
plt.clf()
c = mc2eV/1e6 # convert to MeV
log10En_bins = np.linspace(-1,2,101)
if not np.isscalar(En0):
    counts = np.histogram(np.log10(En0*c), bins=log10En_bins)[0]
else:
    counts = np.histogram(np.log10(En0*c*np.ones((N0,))), bins=log10En_bins)[0]
plt.plot(meandrize(log10En_bins), duplicate(counts))
counts = np.histogram(np.log10(Esat*c), bins=log10En_bins)[0]
plt.plot(meandrize(log10En_bins), duplicate(counts))
for ka in range(3):
    counts = np.histogram(np.log10(Esat[ia[ka]]*c), bins=log10En_bins)[0]
    plt.plot(meandrize(log10En_bins), duplicate(counts))
plt.yscale('log')
plt.xlabel(r'$\log_{10}E$, MeV')
plt.grid(True)
plt.legend(['orig/'+str(Nruns),'sat']+a_str)
plt.title('$h_0 ='+str(h0/1e3)+'$ km')

#%% Check Gjesteland result about alfa>40 degree spectrum
plt.figure(4)
plt.clf()
bins = np.array([35, 120, 500, 2000, 9000, 20000]) # keV
#bins = np.array([40, 100, 400, 1000, 4000, 20000]) #*1e3/mc2eV
for ka in range(3):
    counts = np.histogram(Esat[ia[ka]]*mc2eV/1e3, bins=bins)[0]
    #plt.semilogx(bins[:-1], counts/np.sum(counts), drawstyle='steps-post')
    plt.semilogx(meandrize(bins),duplicate(counts/np.sum(counts)))
    #plt.stairs(bins,counts/np.sum(counts))
plt.xscale('log')
plt.xlabel('$E$, keV')
plt.ylabel('Relative counts')
plt.grid(True)
plt.ylim(0,None)
plt.legend(a_str)
plt.title('$h_0 ='+str(h0/1e3)+'$ km')
