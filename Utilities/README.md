# Utilities

This file has many general-use subroutines which may be in use by other short projects.

A group of subroutines provides colorful output to the terminal and has functionality that intersects with PyPI package [print-color](https://pypi.org/project/print-color/).


