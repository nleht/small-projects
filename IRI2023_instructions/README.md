# International Reference Ionosphere 2023

We provide instructions to use the Fortran code for IRI-2020 model of the ionosphere (May 2023 version) with C, Python and MATLAB. The libraries are created in the self-explanatory `makefile`. Here is how to use it:
```bash
$ make clean          # to remove all files, except the ones downloaded from the web
$ make purge          # also remove the downloaded files (in downloads/)
$ make downloads      # only download stuff from IRI website
$ make c_package      # if you are going to use only C -- this is automatically called for all other options below
$ make c_test         # to test the created library
$ make python_package # create the package *.so file
$ make python_test    # test it
$ make matlab_package # mexa file
$ make matlab_test    # test it
$ make all            # make the C library and Python and MATLAB packages
```
The instructions on what to do with the created files will be in the created `readme_*.txt` files.

The created directory `IRI_data` contains all data necessary to run the IRI model and is relocateable. The most convenient is that you move it to `~/data/IRI`, the default location in the implementation of `IRIClass` in both Python in MATLAB.

In case the FORTRAN source for this version is not available any longer, there is a backup in file `IRI2023May_orig_src.zip`.

The instructions for the previous version (IRI-2016), which include also optional creation of a dynamic library, are available in the [attachment](https://gitlab.com/nleht/stanfordfwm/-/tree/main/external) to the [StanfordFWM](https://gitlab.com/nleht/stanfordfwm/) model. There are also (obsolete) instructions for IRI-2020 model in the parent directory.

